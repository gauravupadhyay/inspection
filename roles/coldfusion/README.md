coldfusion
=========

This will install coldfusion, currently it will install CF8 on windows. this could be adapted to add more tasks to install different versions and for linux as well.


Dependencies
------------

* boto3 and botocore - for windows these need to be installed on the local host running ansible, this is for s3
* apache - the apache role should always be run first otherwise the install will fail.

Variables
------------

* `arch_type` - 32 or 64 this should match what is being used for apache
* `CF_LICENSE` - the license key
* `cf_admin_password` - password from credstash
* `cf_version` - defaults to 8 , currently only 8 is implemnted
* `AWS_ACCESS_KEY_ID` - access key for aws to get items from s3
* `AWS_SECRET_ACCESS_KEY` - secret key for aws to get items from s3 


Example Playbook
------------

```yaml
- name: Playbook to Install Coldfusion 8 on windows
  hosts: windows
  vars:
      AWS_ACCESS_KEY_ID: "{{ AWS_ACCESS_KEY_ID }}"
      AWS_SECRET_ACCESS_KEY: "{{ AWS_SECRET_ACCESS_KEY }}"
      arch_type: "{{ arch_type }}"
  roles:
  - role: apache
  - role: coldfusion
    vars:
      CF_LICENSE: "{{ CF_LICENSE }}"
      cf_admin_password: "{{ cf_admin_password }}"
  pre_tasks:
  - name: Install python libraries for boto3 and botocore
    pip: name="{{ item }}" state=present
    with_items:
      - boto3
      - botocore
    delegate_to: localhost
```

Testing Locally
------------

since this role leverages secrets you need to export the following variables before running:

* `export AWS_ACCESS_KEY_ID=<id>`
* `export AWS_SECRET_ACCESS_KEY=<secret>`

This role uses test kitchen along with the `kitchen-ansible` driver to test locally. Please see the confluence page <placeholder>  for installing dependencies.

* `chef exec bundle install`
  * this will install gems , most should be done already by the dependencies
* `kitchen create`
  * creates the virtualbox servers so that we can run playbooks
* `kitchen converge`
  * runs the ansible plays
* `kitchen verify`
  * execute any tests, this role leverages inspec for testing
* `kitchen destroy`
  * cleanup all related vms
